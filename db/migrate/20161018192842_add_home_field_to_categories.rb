class AddHomeFieldToCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :home_page, :boolean
  end
end
