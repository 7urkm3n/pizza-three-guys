class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  respond_to :html
  before_action :configure_permitted_parameters, if: :devise_controller?


  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name, :avatar])
  end
end
