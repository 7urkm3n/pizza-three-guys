class MenuController < ApplicationController
  def index
  	@categories = Category.includes(:products)
  	@right = @categories.length / 2
  	@left  = @categories.length - @right
  end
end
