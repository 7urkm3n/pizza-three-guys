class HomeController < ApplicationController

	def index
		@coupons = Coupon.all

	  	# @categories = Category.where(home_page: true).includes(:products)
	  	@categories = Category.where(home_page: true).includes(:products).where("products.home_page = ?", true).references(:products)
		@right = @categories.length / 2
		@left  = @categories.length - @right
	end
	
end
