require 'test_helper'

class UnderConstructionControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get under_construction_index_url
    assert_response :success
  end

end
