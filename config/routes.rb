Rails.application.routes.draw do

  devise_for :users, skip: [:registrations], controllers: {
    sessions:      'users/sessions'
    # registrations: 'users/registrations'
  }
 
  devise_scope :user do
    put 'users' => 'users/registrations#update', as: 'registration'
    get 'users/edit' => 'users/registrations#edit', as: 'edit_user_registration'
    delete 'users' => 'users/registrations#destroy', as: 'remove_registration'
  end

  namespace :admin do
    resources :coupons
    resources :products
    resources :categories

    put '/category_popular/:id' => 'categories#category_popular', as: :category_popular
    put '/product_special/:id'  => 'products#product_special', as: :product_special
    put '/product_home_page/:id'  => 'products#product_home_page', as: :product_home_page
  end

  
  get 'menu'       => 'menu#index'
  get 'admin'      => 'admin/products#index'
  # root to: 'home#index'
  root to: 'under_construction#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
